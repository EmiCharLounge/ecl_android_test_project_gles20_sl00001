// 2Dスプライト座標変換

// 例えば想定する(仮想)スクリーンサイズが800x480だったとして、
// 実機の画面が1196x669だった場合、ma.xは1.39375で表示オフセットXは40となる。
// 実機の画面が788x440だった場合、ma.xは0.91667で表示オフセットXは32となる。

// ic=0で左上基点だったとしても、拡大縮小や回転は中心点を基準に行われる。

// 目標サイズ(wh.z,wh.w)は例えば座標50,100から400,300までフィルしたいといった場合に、
// pos.x=50 , pos.y=100 , wh.z=350 , wh.w=200のように設定する(拡大率と回転角は無効)。

// viewは下記で作成
// GLES20.glViewport(表示オフセットX, 表示オフセットY, 実機画面W, 実機画面H);
// Matrix.orthoM(ort, 0, 0.0f, (float)実機画面W, (float)実機画面H, 0.0f, 2.0f, -1010.0f);
// Matrix.setLookAtM(cam, 0, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
// Matrix.setIdentityM(dmy, 0);
// Matrix.multiplyMM(view, 0, cam, 0, dmy, 0);
// Matrix.multiplyMM(view, 0, ort, 0, view, 0);

// viewとidxは一度だけ設定したら以降使いまわせる。

uniform mat4 view;					// 平行投影マトリックス(固定)
uniform vec2 ma;					// ma.x=実機のスクリーンサイズ/仮想スクリーンサイズ , ma.y=仮想スクリーンサイズのアスペクト比(未使用)
uniform vec3 pos;					// 仮想スクリーンに対する座標(画面の左上基点) pos.x=X , pos.y=Y , pos.z=優先順位(Zバッファ用だが現在未反映)
uniform vec4 wh;					// 仮想スクリーン基準の縦横サイズ wh.w=W , wh.h=H / または目標サイズ(拡大回転の影響を受けない。-1.0なら無効) wh.z=W , wh.w=H
uniform vec3 mr;					// 拡大率 mr.x=横倍率 , mr.y=縦倍率 / 回転角 mr.z=deg(0～360度)
uniform vec4 col;					// カラー倍率 r,g,b,a(0.0～1.0)
uniform int ic;						// 0=スプライトの左上基点 , 1=スプライトの中心基点
attribute vec2 a_texCoord;			// UVストリーム
attribute float idx;				// 頂点インデックスストリーム(固定) 0=LT , 1=RT , 2=LB , 3=RB ※intでもいいかも
varying mediump vec2 v_texCoord;
varying mediump vec4 v_col;
void main() {
  vec2 p = pos.xy;
  if(ic == 1) {
    p.x = pos.x - (wh.x / 2.0);
    p.y = pos.y - (wh.y / 2.0);
  }
  if((wh.z >= 0.0)&&(wh.w >= 0.0)) {
    if(idx == 1.0) {
      p.x = pos.x + wh.z - 1.0;
      p.y = pos.y;
    }
    else if(idx == 2.0) {
      p.x = pos.x;
      p.y = pos.y + wh.w - 1.0;
    }
    else if(idx == 3.0) {
      p.x = pos.x + wh.z - 1.0;
      p.y = pos.y + wh.w - 1.0;
    }
    p.x = p.x * ma.x;
    p.y = p.y * ma.x;
    gl_Position = view * vec4(p.x, p.y, pos.z, 1.0);
  }
  else {
    vec2 p2 = p.xy;
    if(idx == 1.0) {
      p2.x = p.x + wh.z - 1.0;
      p2.y = p.y;
    }
    else if(idx == 2.0) {
      p2.x = p.x;
      p2.y = p.y + wh.w - 1.0;
    }
    else if(idx == 3.0) {
      p2.x = p.x + wh.z - 1.0;
      p2.y = p.y + wh.w - 1.0;
    }
    float s = sin(radians(mr.z));
    float c = cos(radians(mr.z));
    float hw = wh.x / 2.0;
    float hh = wh.y / 2.0;
    float cx = hw + p2.x;
    float cy = hh + p2.y;
    if(idx == 0.0) {
      p.x = p2.x - cx;
      p.y = p2.y - cy;
    }
    else if(idx == 1.0) {
      p.x = p2.x - cx + wh.x - 1.0;
      p.y = p2.y - cy;
    }
    else if(idx == 2.0) {
      p.x = p2.x - cx;
      p.y = p2.y - cy + wh.y - 1.0;
    }
    else {
      p.x = p2.x - cx + wh.x - 1.0;
      p.y = p2.y - cy + wh.y - 1.0;
    }
    float lx = p.x * mr.x;
    float ly = p.y * mr.y;
    float x = (lx * c) - (ly * s);
    float y = (lx * s) + (ly * c);
    x = (x + cx) * ma.x;
    y = (y + cy) * ma.x;
    gl_Position = view * vec4(x, y, pos.z, 1.0);
  }
  v_texCoord = a_texCoord;
  v_col = col;
}

